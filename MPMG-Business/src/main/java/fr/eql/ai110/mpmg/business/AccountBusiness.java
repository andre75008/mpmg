package fr.eql.ai110.mpmg.business;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import fr.eql.ai110.mpmg.entity.User;
import fr.eql.ai110.mpmg.ibusiness.AccountIBusiness;
import fr.eql.ai110.mpmg.idao.UserIDao;

@Remote(AccountIBusiness.class)
@Stateless
public class AccountBusiness implements AccountIBusiness{
	
	@EJB
	private UserIDao userDao;

	@Override
	public User connect(String login, String password) {
		String salt = userDao.getSalt(login);
		String hashedPassword = generateHashedPassword(salt, password);
		return userDao.authenticate(login, password);
	}

	private String generateHashedPassword(String salt, String password) {
		String saltedPassword = password + salt;
		String hashedPassword = "";
		
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(saltedPassword.getBytes(StandardCharsets.UTF_8));
			hashedPassword = new String(encodedhash, StandardCharsets.UTF_8);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hashedPassword;
	}

	@Override
	public void create(User user, String password) {
		String salt = generateSalt();
		user.setSalt(salt);
		String saltedPassword = password + salt;
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(saltedPassword.getBytes(StandardCharsets.UTF_8));
			String hashedPassword = new String(encodedhash, StandardCharsets.UTF_8);
			user.setHashedPassword(hashedPassword);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		userDao.add(user);	
			
	}

	private String generateSalt() {
		int leftLimit = 97; // letter 'a'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 3; //salt size
	    Random random = new Random();

	    String salt = random.ints(leftLimit, rightLimit + 1)
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();

	     return salt;
	}

	@Override
	public User getById(int id) {
		
		return userDao.getById(id);
	}

	@Override
	public User update(User user) {
		
		return userDao.update(user);
	}
}
