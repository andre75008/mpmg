package fr.eql.ai110.mpmg.business;

import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.mpmg.entity.Recipe;
import fr.eql.ai110.mpmg.entity.User;
import fr.eql.ai110.mpmg.ibusiness.FavoriteRecipeIBusiness;
import fr.eql.ai110.mpmg.idao.FavoriteRecipeIDao;

@Remote(FavoriteRecipeIBusiness.class)
@Stateless
public class FavoriteRecipeBusiness implements FavoriteRecipeIBusiness {

	@EJB
	private FavoriteRecipeIDao favRecipeDao;

	@Override
	public Set<Recipe> favoriteRecipesByUser(User user) {
		
		return favRecipeDao.findRecipesByUser(user);
	}

	@Override
	public void add(int recipeId, int userId) {
		
		favRecipeDao.add(recipeId, userId);
		
	}
	
	
}
