package fr.eql.ai110.mpmg.business;

import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.mpmg.entity.Recipe;
import fr.eql.ai110.mpmg.entity.User;
import fr.eql.ai110.mpmg.ibusiness.RecipeIBusiness;
import fr.eql.ai110.mpmg.idao.RecipeIDao;

@Remote(RecipeIBusiness.class)
@Stateless
public class RecipeBusiness implements RecipeIBusiness {

	@EJB
	private RecipeIDao recipeDao;
	
	@Override
	public Set<Recipe> getRecipesByUser(User user) {
		return recipeDao.findRecipesByUser(user);
	}
	public List<Recipe> getAll(){
		
		return recipeDao.getAll();
	}
	@Override
	public Set<Recipe> getAllRecipes() {
		Set<Recipe> recipes = null;
		recipes = recipeDao.findAllRecipes();
		recipes.toString();
		return recipes;
	}
	@Override
	public Recipe getById(int id) {
		
		return recipeDao.getById(id);
	}
	@Override
	public List<Recipe> getAllPremiumRecipes() {
		
		return recipeDao.findAllPremiumRecipes();
	}
	@Override
	public List<Recipe> getAllFreeRecipes() {
		
		return recipeDao.findAllFreeRecipes();
	}
	
}
