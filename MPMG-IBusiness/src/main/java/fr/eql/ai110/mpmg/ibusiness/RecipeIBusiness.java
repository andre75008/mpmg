package fr.eql.ai110.mpmg.ibusiness;

import java.util.List;
import java.util.Set;

import fr.eql.ai110.mpmg.entity.Recipe;
import fr.eql.ai110.mpmg.entity.User;

public interface RecipeIBusiness {

	Set<Recipe> getRecipesByUser(User user);
	List<Recipe> getAll();
	Set<Recipe> getAllRecipes();
	Recipe getById(int id);
	List<Recipe> getAllPremiumRecipes();
	List<Recipe> getAllFreeRecipes();
	
}
