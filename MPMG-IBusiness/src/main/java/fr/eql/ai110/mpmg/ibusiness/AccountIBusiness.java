package fr.eql.ai110.mpmg.ibusiness;

import fr.eql.ai110.mpmg.entity.User;

public interface AccountIBusiness {
	
	public User connect(String login, String password);
	public void create(User user, String password);
	public User getById(int id);
	public User update(User user);
}
