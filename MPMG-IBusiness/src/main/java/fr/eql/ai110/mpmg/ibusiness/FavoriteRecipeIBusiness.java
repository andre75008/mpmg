package fr.eql.ai110.mpmg.ibusiness;

import java.util.Set;

import fr.eql.ai110.mpmg.entity.Recipe;
import fr.eql.ai110.mpmg.entity.User;

public interface FavoriteRecipeIBusiness {
	Set<Recipe> favoriteRecipesByUser(User user);
	void add(int recipeId, int userId);

}
