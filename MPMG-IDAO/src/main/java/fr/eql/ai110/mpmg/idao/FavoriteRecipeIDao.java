package fr.eql.ai110.mpmg.idao;

import java.util.Set;

import fr.eql.ai110.mpmg.entity.FavoriteRecipe;
import fr.eql.ai110.mpmg.entity.Recipe;
import fr.eql.ai110.mpmg.entity.User;

public interface FavoriteRecipeIDao extends GenericIDao<FavoriteRecipe>{
	Set<Recipe> findRecipesByUser(User user);
	void add(int i, int j);

}
