package fr.eql.ai110.mpmg.idao;

import fr.eql.ai110.mpmg.entity.User;

public interface UserIDao extends GenericIDao<User>{
	
	User authenticate(String login, String hashedPassword);
	String getSalt(String login);
	
}
