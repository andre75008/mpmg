package fr.eql.ai110.mpmg.idao;

import java.util.List;
import java.util.Set;

import fr.eql.ai110.mpmg.entity.Recipe;
import fr.eql.ai110.mpmg.entity.User;

public interface RecipeIDao extends GenericIDao<Recipe>{
	
	Set<Recipe> findRecipesByUser(User user);
	Set<Recipe> findAllRecipes();
	List<Recipe> findAllPremiumRecipes();
	List<Recipe> findAllFreeRecipes();
}
