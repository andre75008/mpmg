package fr.eql.ai110.mpmg.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "recipe")
public class Recipe implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "title")
	private String title;
	@Column(name = "prep_time")
	private String prepTime;
	@Column(name = "cooking_time")
	private String cookingTime;
	@Column(name = "difficulty")
	private String difficulty;
	@Column(name = "description", length = 15000) //text MySQL
	private String description;
	@Column(name = "picture")
	private String picture;
	@Column(name ="premium")
	private Boolean isPremium;
	@OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<FavoriteRecipe> favRecipes;

	
	
	public Recipe() {
	}
	
	public Recipe(Integer id, String title, String prepTime, String cookingTime, String difficulty, String description,
			String picture, Boolean isPremium, Set<FavoriteRecipe> favRecipes) {
		
		this.id = id;
		this.title = title;
		this.prepTime = prepTime;
		this.cookingTime = cookingTime;
		this.difficulty = difficulty;
		this.description = description;
		this.picture = picture;
		this.isPremium = isPremium;
		this.favRecipes = favRecipes;
		
	}

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getPrepTime() {
		return prepTime;
	}


	public void setPrepTime(String prepTime) {
		this.prepTime = prepTime;
	}


	public String getCookingTime() {
		return cookingTime;
	}


	public void setCookingTime(String cookingTime) {
		this.cookingTime = cookingTime;
	}


	public String getDifficulty() {
		return difficulty;
	}


	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getPicture() {
		return picture;
	}


	public void setPicture(String picture) {
		this.picture = picture;
	}


	public Boolean getIsPremium() {
		return isPremium;
	}


	public void setIsPremium(Boolean isPremium) {
		this.isPremium = isPremium;
	}
	public Set<FavoriteRecipe> getFavRecipes() {
		return favRecipes;
	}

	public void setFavRecipes(Set<FavoriteRecipe> favRecipes) {
		this.favRecipes = favRecipes;
	}

//	@Override
//	public String toString() {
//		return "Recipe [id=" + id + ", title=" + title + ", prepTime=" + prepTime + ", cookingTime=" + cookingTime
//				+ ", difficulty=" + difficulty + ", description=" + description + ", picture=" + picture
//				+ ", isPremium=" + isPremium + ", favRecipes=" + favRecipes + "]";
//	}
	
	
}
