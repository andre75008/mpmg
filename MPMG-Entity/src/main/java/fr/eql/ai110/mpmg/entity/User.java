package fr.eql.ai110.mpmg.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "surname")
	private String surname;
	@Column(name = "login")
	private String login;
	@Column(name = "hashedpassword")
	private String hashedPassword;
	@Column(name = "email")
	private String email;
	@Column(name = "salt")
	private String salt;
	@Column(name = "member")
	private Boolean isMember;
	@Column(name = "subscriber")
	private Boolean isSubscriber;
	@Column(name = "admin")
	private Boolean isAdmin;
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<FavoriteRecipe> favRecipes;
	
	public User() {
		
	}

	public User(Integer id, String name, String surname, String login, String hashedPassword, String email, String salt,
			Boolean isMember, Boolean isSubscriber, Boolean isAdmin, Set<FavoriteRecipe> favRecipes) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.login = login;
		this.hashedPassword = hashedPassword;
		this.email = email;
		this.salt = salt;
		this.isMember = isMember;
		this.isSubscriber = isSubscriber;
		this.isAdmin = isAdmin;
		this.favRecipes = favRecipes;
	}



	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getHashedPassword() {
		return hashedPassword;
	}


	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getSalt() {
		return salt;
	}


	public void setSalt(String salt) {
		this.salt = salt;
	}


	public Boolean getIsMember() {
		return isMember;
	}


	public void setIsMember(Boolean isMember) {
		this.isMember = isMember;
	}


	public Boolean getIsSubscriber() {
		return isSubscriber;
	}


	public void setIsSubscriber(Boolean isSubscriber) {
		this.isSubscriber = isSubscriber;
	}
	
	


	public Boolean getIsAdmin() {
		return isAdmin;
	}


	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Set<FavoriteRecipe> getFavRecipes() {
		return favRecipes;
	}

	public void setFavRecipes(Set<FavoriteRecipe> favRecipes) {
		this.favRecipes = favRecipes;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", surname=" + surname + ", login=" + login + ", hashedPassword="
				+ hashedPassword + ", email=" + email + ", salt=" + salt + ", isMember=" + isMember + ", isSubscriber="
				+ isSubscriber + ", isAdmin=" + isAdmin + "]";
	}




}
