package fr.eql.ai110.mpmg.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="favorite_recipe")
public class FavoriteRecipe implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Recipe recipe;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private User user;
	
	
	
	public FavoriteRecipe() {
		super();
	}

	public FavoriteRecipe(Integer id, Recipe recipe, User user, Integer userId, Integer recipeId, Set<User> users,
			Set<Recipe> recipes) {
		super();
		this.id = id;
		this.recipe = recipe;
		this.user = user;
	}




	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Recipe getRecipe() {
		return recipe;
	}


	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	@Override
	public String toString() {
		return "FavoriteRecipe [id=" + id + ", recipe=" + recipe + ", user=" + user + "]";
	}
	

}
