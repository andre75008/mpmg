package fr.eql.ai110.mpmg.controller;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Set;


import javax.ejb.EJB;
import javax.enterprise.inject.spi.Bean;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.mpmg.entity.FavoriteRecipe;
import fr.eql.ai110.mpmg.entity.Recipe;
import fr.eql.ai110.mpmg.entity.User;
import fr.eql.ai110.mpmg.ibusiness.FavoriteRecipeIBusiness;
import fr.eql.ai110.mpmg.ibusiness.RecipeIBusiness;

@ManagedBean(name="mbRecipe")
@SessionScoped
public class RecipeManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;


	private User connectedUser;
	private Set<Recipe> connectedUserRecipes;
	private Recipe recipe;
	private FavoriteRecipe favRecipe;

	@EJB
	private RecipeIBusiness recipeBusiness;
	@EJB
	private FavoriteRecipeIBusiness favRecipeBusiness;


	public String showFav() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		connectedUser = (User) session.getAttribute("connectedUser");
		connectedUserRecipes = favRecipeBusiness.favoriteRecipesByUser(connectedUser);
		return "/recipebyuser.xhtml?faces-redirection=true";	
	}
	public List<Recipe> getAll(){
		List<Recipe> recipes = null;
		recipes = recipeBusiness.getAll();
		return recipes;
	}
	public Set<Recipe> getAllRecipes(){
		Set<Recipe> recipes = null;
		recipes = recipeBusiness.getAllRecipes();
		return recipes;
	}
	public Recipe getById(int id) {
		Recipe recipe = new Recipe();
		int recipeId = recipe.getId();
		return recipeBusiness.getById(recipeId);
	}
	public String redirectToRecipe(Recipe recipe) {
		this.recipe = recipe;
		return "/recipe.xhtml?faces-redirection=true";
	}
	public List<Recipe> getAllPremiumRecipes(){
		List<Recipe> recipes = null;
		recipes = recipeBusiness.getAllPremiumRecipes();
		return recipes;
	}
	public List<Recipe> getAllFreeRecipes(){
		List<Recipe> recipes = null;
		recipes = recipeBusiness.getAllFreeRecipes();
		return recipes;
	}
	public void add() {
		favRecipeBusiness.add(recipe.getId(), connectedUser.getId());
	}

	public Set<Recipe> getConnectedUserRecipes(){
		return connectedUserRecipes;
	}

	public void setConnectedUserRecipes(Set<Recipe> connectedUserRecipes) {
		this.connectedUserRecipes = connectedUserRecipes;
	}

	public User getConnectedUser() {
		return connectedUser;
	}

	public void setConnectedUser(User connectedUser) {
		this.connectedUser = connectedUser;
	}

	public Recipe getRecipe() {
		return recipe;
	}

	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}
	public FavoriteRecipe getFavRecipe() {
		return favRecipe;
	}
	public void setFavRecipe(FavoriteRecipe favRecipe) {
		this.favRecipe = favRecipe;
	}


}
