package fr.eql.ai110.mpmg.controller;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.eql.ai110.mpmg.entity.FavoriteRecipe;
import fr.eql.ai110.mpmg.entity.Recipe;
import fr.eql.ai110.mpmg.entity.User;
import fr.eql.ai110.mpmg.ibusiness.AccountIBusiness;
import fr.eql.ai110.mpmg.ibusiness.FavoriteRecipeIBusiness;

@ManagedBean(name="mbUser")
@SessionScoped
public class UserManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private User user;
	private Recipe recipe;
	private FavoriteRecipe favRecipe;
	
	@EJB
	private AccountIBusiness accountBusiness;
	
	@EJB
	private FavoriteRecipeIBusiness favBusiness;
	
	
	public User update() {
		return accountBusiness.update(user);
	}
	public void add() {
		favBusiness.add(recipe.getId(), user.getId());
	}


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public Recipe getRecipe() {
		return recipe;
	}
	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}
	public FavoriteRecipe getFavRecipe() {
		return favRecipe;
	}
	public void setFavRecipe(FavoriteRecipe favRecipe) {
		this.favRecipe = favRecipe;
	}
	
	
	
	

}
