package fr.eql.ai110.mpmg.controller;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.mpmg.entity.User;
import fr.eql.ai110.mpmg.ibusiness.AccountIBusiness;

@ManagedBean(name="mbLogin")
@SessionScoped
public class LoginManagedBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String login;
	private String password;
	private String name;
	private String surname;
	private String email;
	private User user;
	
	@EJB
	private AccountIBusiness accountBusiness;
	
	public String create() {
		String forward = "";
		User user = new User();
		user.setLogin(login);
		user.setName(name);
		user.setSurname(surname);
		user.setEmail(email);
		user.setIsMember(true);
		user.setIsSubscriber(false);
		user.setIsAdmin(false);
		
		if(emailIsValid(email)) {
			accountBusiness.create(user, password);
			forward = "/index.xhtml?faces-redirection=true";
		} else {
			FacesMessage efacesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Email incorrect", "Email incorrect");
			FacesContext.getCurrentInstance().addMessage("loginForm:inpEmail", efacesMessage);
			user = null;
			forward = "/create.xhtml?faces-redirection=false";
		}
		return forward;
	}
	
	public String connect() {
		String forward = null;
		user = accountBusiness.connect(login, password);
		if(user != null) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
			session.setAttribute("connectedUser", user);
			forward = "/index.xhtml?faces-redirection=true";
		}else {
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"Identifiant et/ou mot de passe incorrect(s)",
					"Identifiant et/ou mot de passe incorrect(s)"
					);
			FacesContext.getCurrentInstance().addMessage("loginForm:inpLogin", facesMessage);
			FacesContext.getCurrentInstance().addMessage("loginForm:inpPassword", facesMessage);
			forward = "/login.xhtml?faces-redirection=false";
		}
		return forward;
	}
	
	public boolean isConnected() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		user = (User) session.getAttribute("connectedUser");
		return user != null;
	}
	public boolean hasAccount() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		user = (User) session.getAttribute("connectedUser");
		return user != null;
	}
	
	public String disconnect() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
							.getExternalContext().getSession(true);
		session.invalidate();
		user = null;
		return "/login.xhtml?faces-redirection=true";
	}
	
	private boolean emailIsValid(String email) {
		
		boolean emailIsValid = false;
		int upperCount = 0;
		
		for (int i = 0; i < email.length(); i++) {
			char ch = email.charAt(i);
			if(Character.isUpperCase(ch))
				upperCount++;
		}
		if(email.contains("@") && upperCount > 0) {
			emailIsValid = true;
		} else {
			emailIsValid = false;
		}
			
		return emailIsValid;
	}
	public void  update() {
		user = accountBusiness.update(user);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	

}
