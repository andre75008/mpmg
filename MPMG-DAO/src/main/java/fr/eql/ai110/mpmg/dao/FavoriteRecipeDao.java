package fr.eql.ai110.mpmg.dao;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.mpmg.entity.FavoriteRecipe;
import fr.eql.ai110.mpmg.entity.Recipe;
import fr.eql.ai110.mpmg.entity.User;
import fr.eql.ai110.mpmg.idao.FavoriteRecipeIDao;

@Remote(FavoriteRecipeIDao.class)
@Stateless
public class FavoriteRecipeDao extends GenericDao<FavoriteRecipe> implements FavoriteRecipeIDao{


	
	public  void add(int recipeId, int userId) {
		Query query = entMag.createQuery("INSERT INTO FavoriteRecipe (recipe.id, user.id) VALUES ('recipe.id', 'user.id')");
		

	}


	public boolean delete(FavoriteRecipeIDao t) {
		// TODO Auto-generated method stub
		return false;
	}


	public FavoriteRecipeIDao update(FavoriteRecipeIDao t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FavoriteRecipe getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Recipe> findRecipesByUser(User user) {
		Query query = entMag.createQuery("SELECT f.recipe FROM FavoriteRecipe f WHERE f.user = :userParam");
		query.setParameter("userParam", user);
		return new HashSet<Recipe>(query.getResultList());
	}





}
