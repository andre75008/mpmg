package fr.eql.ai110.mpmg.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.eql.ai110.mpmg.idao.GenericIDao;

public abstract class GenericDao<T> implements GenericIDao<T> {
	
	@PersistenceContext(unitName = "MPMGPU")
	protected EntityManager entMag;
	
	public T add(T t) {
		entMag.persist(t);
		return t;
	}
	
	public boolean delete(T t) {
		boolean isRemoved;
		try {
			t = entMag.merge(t);
			entMag.remove(t);
			isRemoved = true;
			
		} catch (Exception e) {
			e.printStackTrace();
			isRemoved = false;
		}
		return isRemoved;
	}
	
	public T update(T t) {
		System.out.println("user " + t.toString());
		entMag.merge(t);
		System.out.println("updated " + t.toString());
		return t;
	}
	
	public T getById(int id) {
		T t = null;
		try {
			String className= ((ParameterizedType) getClass().
					getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
			Class<?> clazz;
			clazz = Class.forName(className);
			t = (T) entMag.find(clazz, id);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return t;
	}
	public List<T> getAll() {
		List<T> objects = null;
		try {
			String className= ((ParameterizedType) getClass().
					getGenericSuperclass()).getActualTypeArguments()[0].getTypeName();
			Class<?> clazz;
			clazz = Class.forName(className);
			Query query = entMag.createQuery("FROM " + clazz.getName());
			objects = query.getResultList();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return objects;
	}
	

}
