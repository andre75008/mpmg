package fr.eql.ai110.mpmg.dao;

import java.util.List;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.mpmg.entity.User;
import fr.eql.ai110.mpmg.idao.UserIDao;

@Remote(UserIDao.class)
@Stateless
public class UserDao extends GenericDao<User> implements UserIDao{

	@Override
	public User authenticate(String login, String hashedPassword) {
		User user = null;
		List<User> users;
		Query query = entMag.createQuery("SELECT u FROM User u WHERE u.login = :loginParam" + 
										" AND u.hashedPassword = :hashedPasswordParam");
		query.setParameter("loginParam", login);
		query.setParameter("hashedPasswordParam", hashedPassword);
		users = query.getResultList();
		if (users.size() > 0) {
			user = users.get(0);
		}
		return user;
	}

	@Override
	public String getSalt(String login) {
		Query query = entMag.createQuery("SELECT u.salt FROM User u WHERE u.login =:loginParam");
		query.setParameter("loginParam", login);
		return (String)query.getSingleResult();
	}

}
