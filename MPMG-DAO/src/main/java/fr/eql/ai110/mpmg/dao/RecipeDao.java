package fr.eql.ai110.mpmg.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.mpmg.entity.Recipe;
import fr.eql.ai110.mpmg.entity.User;
import fr.eql.ai110.mpmg.idao.RecipeIDao;

@Remote(RecipeIDao.class)
@Stateless
public class RecipeDao extends GenericDao<Recipe> implements RecipeIDao {

	@Override
	public Set<Recipe> findRecipesByUser(User user) {
		Query query = entMag.createQuery("SELECT r FROM Recipe r WHERE r.user = :userParam");
		query.setParameter("userParam", user);
		return new HashSet<Recipe>(query.getResultList());
	}


	@Override
	public Set<Recipe> findAllRecipes() {
		Query query = entMag.createQuery("SELECT r FROM Recipe r");
		return new HashSet<Recipe>(query.getResultList());
	}
	
	@Override
	public Recipe getById(int id) {
		Recipe recipe = null;
		List<Recipe> recipes;
		Query query = entMag.createQuery("SELECT r FROM Recipe r WHERE r.id = :idParam");
		query.setParameter("idParam", id);
		recipes = query.getResultList();
		if (recipes.size() > 0) {
			recipe = recipes.get(0);
		}
		return recipe;
		
	}


	@Override
	public List<Recipe> findAllPremiumRecipes() {
		Query query = entMag.createQuery("SELECT r FROM Recipe r WHERE r.isPremium = true");
		return query.getResultList();
	}


	@Override
	public List<Recipe> findAllFreeRecipes() {
		Query query = entMag.createQuery("SELECT r FROM Recipe r WHERE r.isPremium = false");
		return query.getResultList();
	}
}
