INSERT INTO `user` (name, surname, login, hashedpassword, email, salt, member, subscriber, admin) VALUES ('Bob', 'Boby', 'bob', 'bob', 'bob@Lebob.com', null, true, true, false);
INSERT INTO `user` (name, surname, login, hashedpassword, email, salt, member, subscriber, admin) VALUES ('Plop', 'Plop', 'plop', 'plop', 'plop@Leplop.com', null, true, false, false);
INSERT INTO `user` (name, surname, login, hashedpassword, email, salt, member, subscriber, admin) VALUES ('Titi', 'Titi', 'titi', 'titi', 'titi@Letiti.com', null, true, false, false);
INSERT INTO `user` (name, surname, login, hashedpassword, email, salt, member, subscriber, admin) VALUES ('Tata', 'Tata', 'tata', 'tata', 'tata@Letata.com', null, true, false, false);
INSERT INTO `user` (name, surname, login, hashedpassword, email, salt, member, subscriber, admin) VALUES ('Toto', 'Toto', 'toto', 'toto', 'toto@Letoto.com', null, true, false, false);
INSERT INTO `user` (name, surname, login, hashedpassword, email, salt, member, subscriber, admin) VALUES ('Tâtâ', 'Tâtâ', 'tata', 'tata', 'tata@Letata.com', null, true, false, false);


INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Fondant chocolat', '15 mn', '10 mn', 'facile', 'blablabla', 'fondant.jpg', false);
INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Pâte feuilletée', '2 jours', '45 mn', 'difficile', 'blablabla', 'patefeuilletee.jpg', false);
INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Pâte à choux', '20 mn', '45 mn', 'intermédiaire', 'blablabla', 'pateachoux.jpg', false);
INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Chef levain', '10 mn', null, 'facile', 'blablabla', 'levain.jpg', false);
INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Tradition', '15 mn', '25 mn', 'intermédiaire', 'blablabla', 'tradition.jpg', false);
INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Brioche', '15 mn', '25 mn', 'intermédiaire', 'blablabla', 'brioche.jpg', false);
INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Paris Brest', '30 mn', '50 mn', 'intermédiaire', 'blablabla', 'parisBrest.jpg', true);
INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Gateau Russe', '40 mn', '50 mn', 'facile', 'blablabla', 'gateaurusse.jpg', true);
INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Fraisier', '50 mn', '1 h 15 mn', 'intermédiaire', 'blablabla', 'fraisier.jpg', true);
INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Kouign amann', '1 h 30 mn', '50 mn', 'difficile', 'blablabla', 'kouignamann.jpg', true);
INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Tourte de seigle', '1h', '45 mn', 'difficile', 'blablabla', 'tourteseigle.jpg', true);
INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Opéra', '45 mn', '45 mn', 'intermédiaire', 'blablabla', 'opera.jpg', true);
INSERT INTO `recipe` (title, prep_time, cooking_time, difficulty, description, picture, premium) VALUES ('Canelé', '30 mn', '50 mn', 'facile', 'blablabla', 'canele.jpg', true);

INSERT INTO `favorite_recipe` (recipe_id, user_id) VALUES (1, 1);
INSERT INTO `favorite_recipe` (recipe_id, user_id) VALUES (2, 1);
INSERT INTO `favorite_recipe` (recipe_id, user_id) VALUES (3, 1);
INSERT INTO `favorite_recipe` (recipe_id, user_id) VALUES (4, 1);
INSERT INTO `favorite_recipe` (recipe_id, user_id) VALUES (5, 1);
INSERT INTO `favorite_recipe` (recipe_id, user_id) VALUES (6, 1);
INSERT INTO `favorite_recipe` (recipe_id, user_id) VALUES (1, 2);
INSERT INTO `favorite_recipe` (recipe_id, user_id) VALUES (2, 2);
INSERT INTO `favorite_recipe` (recipe_id, user_id) VALUES (3, 2);
INSERT INTO `favorite_recipe` (recipe_id, user_id) VALUES (4, 2);
INSERT INTO `favorite_recipe` (recipe_id, user_id) VALUES (5, 2);
INSERT INTO `favorite_recipe` (recipe_id, user_id) VALUES (6, 2);